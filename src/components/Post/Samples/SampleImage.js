import React, {Component} from 'react';
import { Text, View, Image} from 'react-native';
import { RkCard, RkText } from 'react-native-ui-kitten';

class SampleImage extends Component{
  render() {
    return (
        <RkCard rkType="heroImage"  style={{"marginTop": 10, "marginBottom" : 10}}>
        <View rkCardHeader>
          <RkText rkType="xlarge primary">Header</RkText>
        </View>
        <Image rkCardImg source={require('../../../../img/sea.jpeg')}/>
        <View rkCardContent>
          <RkText> quick brown fox jumps over the lazy dog</RkText>
        </View>
        <View rkCardFooter rkType="">
          <RkText rkType="small info">232 Comments</RkText>
          <RkText rkType="primary subtitle">31.5k Upvotes</RkText>
          <RkText rkType="large success">Up</RkText>
        </View>
        </RkCard>
    );
  }
}

export default SampleImage;

// const styles = StyleSheet.create({
//   container: {
//     padding: 10,
//     // flex: 1,
//     // justifyContent: 'center',
//     // alignItems: 'center',
//     backgroundColor: '#F5FCFF',
//   }
// });
