import React, { Component } from 'react';
import { RkCard, RkText } from 'react-native-ui-kitten';
import { View, Text } from 'react-native'

class SampleText extends Component {

    render() {
        return (
        <RkCard rkType = "heroImage" style = {{ "marginTop": 10, "marginBottom": 10 }} >
            <View rkCardHeader >
                <RkText rkType="xlarge">Header</RkText>
            </View>
            <View rkCardContent >
                <RkText rkType="info small">Lorem ipsum dolor sit amet,
                    consectetur adipiscing elit, sed do
                    eiusmod tempor incididunt ut labore
                    et dolore magna aliqua. Ut enim ad
                    minim veniam, quis nostrud exercitation
                    ullamco laboris nisi ut aliquip ex ea
                    commodo consequat. Duis aute irure
                    dolor in reprehenderit in voluptate
                    velit esse cillum dolore eu fugiat
                    nulla pariatur. Excepteur sint occaecat
                    cupidatat non proident, sunt in culpa
                    qui officia deserunt mollit anim id
                    est laborum.
                </RkText>
            </View>
            <View rkCardFooter>
                <RkText rkType = "small info" > 232 Comments </RkText>
                <RkText rkType = "primary subtitle" > 31.5 k Upvotes </RkText>
                <RkText rkType = "large success" > Up </RkText>
            </View>
        </RkCard>
        )
    }

}

export default SampleText;