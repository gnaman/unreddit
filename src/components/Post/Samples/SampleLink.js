import React, {Component} from 'react';
import { Text, View, Image} from 'react-native';
import { RkCard, RkText } from 'react-native-ui-kitten';

class SampleLink extends Component{
  render() {
    var url = 'https://www.google.com/'
    return (
        <RkCard rkType="heroImage"  style={{"marginTop": 10, "marginBottom" : 10}}>
        <View rkCardHeader>
          <RkText rkType="xlarge primary">Header is too long for the card to fit in so I want to see what happens when this is the case</RkText>
        </View>
        <View rkCardHeader style={{"padding" : 0, "margin" : 0}}>
        <RkText rkType="small info">{url.replace('http://','').replace('https://','').split(/[/?#]/)[0]}</RkText>
        </View>
        <Image rkCardImg source={require('../../../../img/sea.jpeg')}/>
        <View rkCardContent>
          <Text> quick brown fox jumps over the lazy dog</Text>
        </View>
        <View rkCardFooter rkType="">
          <RkText rkType="small info">232 Comments</RkText>
          <RkText rkType="primary subtitle">31.5k Upvotes</RkText>
          <RkText rkType="large success">Up</RkText>
        </View>
        </RkCard>
    );
  }
}

export default SampleLink;

// const styles = StyleSheet.create({
//   container: {
//     padding: 10,
//     // flex: 1,
//     // justifyContent: 'center',
//     // alignItems: 'center',
//     backgroundColor: '#F5FCFF',
//   }
// });
