import React, {Component} from 'react';
import { StyleSheet, ScrollView } from 'react-native';
import SampleImage from './src/components/Post/Samples/SampleImage';
import SampleText from './src/components/Post/Samples/SampleText';
import SampleLink from './src/components/Post/Samples/SampleLink';

// type Props = {};
// export default class App extends Component<Props> {
export default class App extends Component{
  render() {
    return (
      <ScrollView style={styles.container}>
        <SampleImage></SampleImage>
        <SampleText></SampleText>
        <SampleLink></SampleLink>
        <SampleImage></SampleImage>
        <SampleImage></SampleImage>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    padding: 10,
    backgroundColor: '#F5FCFF',
  }
});